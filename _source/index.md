# Welcome to TickMC Wiki

This is a guide to TickMC, a server owned by 0TickPulse. It is a Minecraft server that incorporates custom content and vanilla content alike.

IP: `tick-mc.net`

## Contents

``` {toctree}
:hidden:

self
```

``` {toctree}
:maxdepth: 2
:titlesonly:
combat_system/index
```
