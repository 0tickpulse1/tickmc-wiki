# Elements

TickMC has a complex system that implements the concept of elemental combat.

## ![earth](../media/element_earth.png) Earth

This element symbolizes both the rocks of the earth and the greenery of the earth. Therefore, gear with this element is generally defensive or healing.

## ![ender](../media/element_ender.png) Ender

Ender is a magical element of unknown origin. It synergizes extremely well with highly concentrated energy. Therefore, gear with this element are highly related to mana.

## ![fire](../media/element_fire.png) Fire

Fire is a common element that deals immense damage. Gear with this element deal high and consistent damage.

## ![ice](../media/element_ice.png) Ice

Ice can freeze the body, rendering one powerless regardless of their power. Gear with this element can freeze or slow enemies.

## ![light](../media/element_light.png) Light

Another element with unknown origin. This element generally deals the highest damage.

## ![shadow](../media/element_shadow.png) Shadow

Said to be the "other side" of light. Gear with this element generally has high burst damage.

## ![water](../media/element_water.png) Water

Water has remarkable healing properties.

## ![wind](../media/element_wind.png) Wind

Wind weapons have exceptional crowd control and suction power.
